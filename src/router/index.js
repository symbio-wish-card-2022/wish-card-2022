import Vue from 'vue'
import Router from 'vue-router'
import Landing from '@/components/Landing'
import Mail from '@/components/Mail'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'landing',
      component: Landing
    },
    {
      path: '/mail',
      name: 'mail',
      component: Mail
    }
  ]
})
