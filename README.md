# symbio-wish-card

> Symbio - Wish Card

## Build Setup

``` bash
# setup environment
npm install -g @vue/cli
npm install -g @vue/cli-init

# project init
vue init webpack symbio-wish-card
cd symbio-wish-card

# install dependencies
npm install --save vuex vue-router vue-resource
npm install --save style-loader style-resources-loader node-fetch
npm install --save-dev sass@1.22.10 sass-loader@7.2.0

# complete 'webpack.base.conf.js' to be able to compile SCSS stylesheets
module: {
  rules: [
    [ ... ]
    {
      test: /\.s[ac]ss$/i,
      use: [
        "style-loader",
        {
          loader: "css-loader",
          options: {
            sourceMap: true,
          },
        },
        {
          loader: "sass-loader",
          options: {
            implementation: require.resolve("sass"),
            sourceMap: true,
            sassOptions: {
              outputStyle: "compressed",
            },
          },
        },
      ],
    },
    [ ... ]
  ]
}

# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
